<?php

/**
 * @file
 * Cloud Orchestrator Installer.
 */

use Drupal\cloud_orchestrator\Installer\Form\ModuleConfigureForm;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_theme().
 */
function cloud_orchestrator_theme($existing, $type, $theme, $path) {
  return [
    'cloud_orchestrator_dashboard' => [
      'variables' => [
        'top' => NULL,
        'content' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function cloud_orchestrator_form_cloud_admin_settings_alter(array &$form, FormStateInterface $form_state): void {
  $cloud_configs = \Drupal::entityTypeManager()
    ->getStorage('cloud_config_type')
    ->loadMultiple();

  if (empty($cloud_configs)) {
    return;
  }

  $config = \Drupal::configFactory()->get('cloud_orchestrator.settings');
  $form['cloud_orchestrator'] = [
    '#type' => 'details',
    '#open' => TRUE,
    '#title' => t('Cloud Orchestrator'),
  ];

  $providers = [];
  foreach ($cloud_configs ?: [] as $cloud_config) {
    $providers[$cloud_config->id()] = $cloud_config->label();
  }
  $form['cloud_orchestrator']['disable_cloud_orchestrator_dashboard_tabs'] = [
    '#title' => t('Disable dashboard tabs'),
    '#description' => t('Check cloud service provider(s) to disable their blocks from the dashboard.'),
    '#type' => 'checkboxes',
    '#options' => $providers,
    '#default_value' => $config->get('disable_cloud_orchestrator_dashboard_tabs') ?: [],
  ];
  $form['#submit'][] = 'cloud_orchestrator_form_cloud_admin_settings_form_submit';
}

/**
 * Submit function for cloud_orchestrator_form_cloud_admin_settings_alter().
 *
 * @param array $form
 *   An associative array containing the structure of the form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the form.
 */
function cloud_orchestrator_form_cloud_admin_settings_form_submit(array $form, FormStateInterface $form_state): void {
  $config = \Drupal::configFactory()->getEditable('cloud_orchestrator.settings');
  $config->set('disable_cloud_orchestrator_dashboard_tabs', $form_state->getValue('disable_cloud_orchestrator_dashboard_tabs'));
  $config->save();
}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function cloud_orchestrator_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  // Add a placeholder as example that one can choose an arbitrary site name.
  $form['site_information']['site_name']['#default_value'] = t('Cloud Orchestrator');
}

/**
 * Implements hook_install_tasks().
 */
function cloud_orchestrator_install_tasks() {
  $tasks = [
    'cloud_orchestrator_module_configure_form' => [
      'display_name' => t('Additional configurations'),
      'type' => 'form',
      'function' => ModuleConfigureForm::class,
    ],
  ];
  return $tasks;
}

/**
 * Implements hook_install_tasks_alter().
 */
function cloud_orchestrator_install_tasks_alter(array &$tasks, array $install_state) {
  $tasks['install_finished']['function'] = 'cloud_orchestrator_after_install_finished';
}

/**
 * Redirect to cloud_config add form after installer finishes.
 *
 * @param array $install_state
 *   The current install state.
 *
 * @return array
 *   A renderable array with a redirect header.
 */
function cloud_orchestrator_after_install_finished(array &$install_state) {
  global $base_url;
  $add_cloud_config_url = $base_url . '/?tour';
  install_finished($install_state);

  $output = [];

  // Clear all messages.
  \Drupal::messenger()->deleteAll();

  $output = [
    '#title' => t('Cloud Orchestrator'),
    'info' => [
      '#markup' => t('<p>Congratulations, you have installed Cloud Orchestrator!</p><p>If you are not redirected to the front page in 5 seconds, Please <a href="@url">click here</a> to proceed to your installed site.</p>', [
        '@url' => $add_cloud_config_url,
      ]),
    ],
    '#attached' => [
      'http_header' => [
        ['Cache-Control', 'no-cache'],
      ],
    ],
  ];

  $meta_redirect = [
    '#tag' => 'meta',
    '#attributes' => [
      'http-equiv' => 'refresh',
      'content' => '0;url=' . $add_cloud_config_url,
    ],
  ];
  $output['#attached']['html_head'][] = [$meta_redirect, 'meta_redirect'];

  return $output;
}

/**
 * Utility function to update yml files.
 *
 * @param array $files
 *   Array of yml files.
 * @param string $dir
 *   Directory where yml files are found.
 * @param string $type
 *   Module or profile.
 * @param string $name
 *   Module or profile name.
 */
function cloud_orchestrator_update_yml_definitions(array $files, $dir = '', $type = 'profile', $name = 'cloud_orchestrator') {
  $config_manager = \Drupal::service('config.manager');
  $config_path = realpath(\Drupal::service('extension.path.resolver')->getPath($type, $name)) . '/config/' . $dir;

  foreach ($files as $file) {
    $filename = $config_path . '/' . $file;
    $file = file_get_contents($filename);
    if (!$file) {
      continue;
    }
    $value = Yaml::decode($file);
    $type = $config_manager->getEntityTypeIdByName(basename($filename));
    $entity_manager = $config_manager->getEntityTypeManager();
    $definition = $entity_manager->getDefinition($type);
    $id_key = $definition->getKey('id');
    $id = $value[$id_key];
    $entity_storage = $entity_manager->getStorage($type);
    $entity = $entity_storage->load($id);
    if ($entity) {
      $entity = $entity_storage->updateFromStorageRecord($entity, $value);
      $entity->save();
    }
    else {
      $entity = $entity_storage->createFromStorageRecord($value);
      $entity->save();
    }
  }
}

/**
 * Implements template_preprocess_layout().
 */
function cloud_orchestrator_preprocess_layout(&$variables) {
  // Set the tab_labels for the twig template.
  $variables['tab_labels'] = !empty($variables['content']['#tab_labels'])
    ? $variables['content']['#tab_labels']
    : [];
}
