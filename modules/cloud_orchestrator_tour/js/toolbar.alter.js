(function escapeAdminPathHandler() {
  const escapeAdminPath = sessionStorage.getItem('escapeAdminPath');
  if (escapeAdminPath.indexOf('?tour') !== -1) {
    const split = escapeAdminPath.split('?tour');
    if (split.length === 2) {
      sessionStorage.setItem('escapeAdminPath', split[0]);
      // console.log(split[0]);
    }
  }
})();
