<?php

namespace Drupal\cloud_orchestrator\Controller;

use Drupal\cloud\Traits\CloudContentEntityTrait;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller to build dashboard.
 */
class DashboardController extends ControllerBase implements DashboardControllerInterface {

  use CloudContentEntityTrait;

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   EntityTypeManagerInterface object.
   */
  public function __construct(BlockManagerInterface $block_manager, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->blockManager = $block_manager;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Dependency Injection.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Instance of ContainerInterface.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block'),
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDashboard(): array {
    $content = [];
    $cloud_configs = [];
    try {
      $cloud_configs = $this->entityTypeManager
        ->getStorage('cloud_config_type')
        ->loadMultiple();
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return [];
    }
    foreach ($cloud_configs ?: [] as $cloud_config) {
      $method_name = $this->getBlockMethodName($cloud_config->id());
      // Check if the block needs hiding.
      if ($this->hideCloudConfigBlocks($cloud_config->id()) === TRUE) {
        continue;
      }
      $content[] = $this->$method_name();
    }

    return [
      '#theme' => 'cloud_orchestrator_dashboard',
      '#top' => $this->getBlock('cloud_config_location'),
      '#content' => $content,
      '#attached' => [
        'library' => [
          'cloud_orchestrator/twocol_tabbed_bricks',
        ],
      ],
    ];
  }

  /**
   * Helper method to get the method that is creating the block content.
   *
   * @param string $cloud_config
   *   Cloud config string.
   *
   * @return string
   *   Method string.
   */
  private function getBlockMethodName(string $cloud_config): string {
    $name = self::getCamelCaseWithoutWhitespace($cloud_config);
    // If the name is Openstack, change it to OpenStack to keep the naming
    // of OpenStack consistent with other methods.
    if ($name === 'Openstack') {
      $name = 'OpenStack';
    }
    return "get{$name}Blocks";
  }

  /**
   * Check if a block should be displayed.
   *
   * @param string $cloud_config
   *   The cloud_config to check.
   *
   * @return bool
   *   TRUE to hide, FALSE otherwise.
   */
  private function hideCloudConfigBlocks(string $cloud_config): bool {
    if ($this->moduleHandler()->moduleExists($cloud_config) === FALSE) {
      return TRUE;
    }

    $disabled_providers = $this->configFactory->get('cloud_orchestrator.settings')
      ->get('disable_cloud_orchestrator_dashboard_tabs');

    return !empty($disabled_providers) && in_array($cloud_config, $disabled_providers) ? TRUE : FALSE;
  }

  /**
   * Helper to get OpenStack blocks.
   *
   * @return array
   *   Array of blocks.
   */
  protected function getOpenStackBlocks(): array {
    $openstack_blocks = [];
    $openstack_blocks['label'] = $this->t('OpenStack');
    $openstack_blocks['top'] = $this->getBlock(
      'openstack_limit_summary_block',
      [
        'cloud_context' => '',
      ],
    );
    $openstack_blocks['middle'] = $this->getBlock(
      'openstack_resources_block',
      [
        'cloud_context' => '',
      ],
    );
    return $openstack_blocks;
  }

  /**
   * Helper to get VMware blocks.
   *
   * @return array
   *   Array of blocks.
   */
  protected function getVmwareBlocks(): array {
    $vmware_blocks = [];
    $vmware_blocks['label'] = $this->t('VMware');
    $vmware_blocks['top'] = $this->getBlock(
      'vmware_resources_block',
      [
        'cloud_context' => '',
      ],
    );
    $vmware_blocks['first_above'][] = $this->getBlock(
      'vmware_host_block',
      [
        'cloud_context' => '',
      ]
    );
    $vmware_blocks['second_above'][] = $this->getBlock(
      'vmware_vm_block',
      [
        'cloud_context' => '',
      ]
    );
    return $vmware_blocks;
  }

  /**
   * Helper to get Terraform blocks.
   *
   * @return array
   *   Array of blocks.
   */
  protected function getTerraformBlocks(): array {
    $terraform_blocks = [];
    $terraform_blocks['label'] = $this->t('Terraform');
    $terraform_blocks['top'] = $this->getBlock(
      'terraform_workspace_block',
      [
        'cloud_context' => '',
      ]
    );
    return $terraform_blocks;
  }

  /**
   * Helper to get AWS blocks and set them in regions for the twig template.
   *
   * @return array
   *   Array of blocks.
   */
  protected function getAwsCloudBlocks(): array {
    $aws_blocks = [];
    $aws_blocks['label'] = $this->t('AWS');
    $aws_blocks['top'] = $this->getBlock(
      'aws_cloud_resources_block',
      [
        'cloud_context' => '',
      ]);
    $aws_blocks['first_above'][] = $this->getBlock('aws_cloud_launch_instance_block');
    $aws_blocks['first_above'][] = $this->getBlock('aws_cloud_long_running_instances_block');
    $aws_blocks['first_above'][] = $this->getBlock(
      'aws_cloud_low_utilization_instances_block',
      [
        'low_utilization_block_cache_interval' => 3600,
      ]);
    $aws_blocks['first_above'][] = $this->getBlock('aws_cloud_detached_volumes_block');
    $aws_blocks['second_above'][] = $this->getBlock('aws_cloud_unused_elastic_ips_block');
    $aws_blocks['second_above'][] = $this->getBlock(
      'aws_cloud_snapshots_block',
      [
        'cloud_context' => '',
        'snapshot_block_type' => 'disassociated_snapshots',
      ]);
    $aws_blocks['second_above'][] = $this->getBlock(
      'aws_cloud_snapshots_block',
      [
        'cloud_context' => '',
        'snapshot_block_type' => 'stale_snapshots',
      ]);
    return $aws_blocks;
  }

  /**
   * Helper to get K8s blocks and set them in regions for the twig template.
   *
   * @return array
   *   Array of blocks.
   */
  protected function getK8sBlocks(): array {
    $k8s_blocks = [];
    $k8s_blocks['label'] = $this->t('K8s');
    $k8s_blocks['top'] = $this->getBlock(
      'k8s_resources_block',
      [
        'cloud_context' => '',
      ]);
    $k8s_blocks['first_above'] = $this->getBlock(
      'k8s_node_allocated_resources',
      [
        'display_view_k8s_node_list_only' => 0,
        'display_in_admin_pages' => 0,
      ]);
    $k8s_blocks['second_above'][] = $this->getBlock(
      'k8s_node_heatmap',
      [
        'display_view_k8s_node_list_only' => 0,
        'display_in_admin_pages' => 0,
      ]);
    $k8s_blocks['second_above'][] = $this->getBlock(
      'k8s_node_costs',
      [
        'display_in_admin_pages' => 0,
        'display_view_k8s_node_list_only' => 0,
      ]);
    $k8s_blocks['middle'][] = $this->getBlock(
      'k8s_namespace_costs_chart',
      [
        'display_in_admin_pages' => 0,
        'display_view_k8s_namespace_list_only' => 0,
        'aws_cloud_chart_period' => '1',
        'aws_cloud_chart_ec2_cost_type' => 'ri_one_year',
      ]);
    $k8s_blocks['middle'][] = $this->getBlock(
      'k8s_namespace_costs',
      [
        'display_view_k8s_namespace_list_only' => 0,
        'aws_cloud_ec2_cost_type' => 'ri_one_year',
      ]);
    return $k8s_blocks;
  }

  /**
   * Helper function to build a block.
   *
   * @param string $block_id
   *   Block id to build.
   * @param array $config
   *   Configuration for the block.
   *
   * @return array
   *   Render array of the block.
   */
  protected function getBlock(string $block_id, array $config = []): array {
    try {
      /** @var \Drupal\Core\Block\BlockPluginInterface $block */
      $block = $this->blockManager->createInstance($block_id, $config);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return [];
    }
    if ($block->access($this->currentUser())) {
      $block_render_array = [
        '#theme' => 'block',
        '#attributes' => [],
        '#contextual_links' => [],
        '#configuration' => $block->getConfiguration(),
        '#plugin_id' => $block->getPluginId(),
        '#base_plugin_id' => $block->getBaseId(),
        '#derivative_plugin_id' => $block->getDerivativeId(),
      ];
      $content = $block->build();

      // If the block is empty, instead of trying to render the block
      // correctly return just #cache, so that the render system knows the
      // reasons (cache contexts & tags) why this block is empty.
      if (Element::isEmpty($content)) {
        $block_render_array = [];
        $cacheable_metadata = CacheableMetadata::createFromObject($block_render_array);
        $cacheable_metadata->applyTo($block_render_array);
        if (isset($content['#cache'])) {
          $block_render_array['#cache'] += $content['#cache'];
        }
      }
      $block_render_array['content'] = $content;
      return $block_render_array;
    }
    return [];
  }

}
